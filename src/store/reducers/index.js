import { combineReducers } from 'redux';

import {
    ALBUMS_GET,
    FAVORITES_ADD,
    FAVORITES_REMOVE,
    HISTORY_SET_ALBUM,
    HISTORY_SET_FILTER,
    HISTORY_SET_PHOTO
} from '../action-types';

const albumsReducer = (state = {}, { payload, type }) => {
    switch (type) {
        case ALBUMS_GET:
            return payload;
        default:
            return state;
    }
};

const favoritesReducer = (state = {}, { payload, type }) => {
    switch (type) {
        case FAVORITES_ADD:
            return {
                ...state,
                [payload.id]: payload
            };
        case FAVORITES_REMOVE:
            const newState = { ...state };
            delete newState[payload];
            return newState;
        default:
            return state;
    }
};

const historyReducer = (state = { albumId: 'favorites', photo: null, titleFilter: '' }, { payload, type }) => {
    switch (type) {
        case HISTORY_SET_ALBUM:
            return {
                ...state,
                albumId: payload
            };
        case HISTORY_SET_FILTER:
            return {
                ...state,
                titleFilter: payload
            };
        case HISTORY_SET_PHOTO:
            return {
                ...state,
                photo: payload
            };
        default:
            return state;
    }
};

export default combineReducers(
    {
        albums: albumsReducer,
        favorites: favoritesReducer,
        history: historyReducer
    }
);
