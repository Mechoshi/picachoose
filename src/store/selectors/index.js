export const selectPhotosToDisplay = state => {
    const { albums, history: { albumId, titleFilter }, favorites } = state;
    let album;

    if (albumId === 'favorites') {
        album = Object.values(favorites);
    } else if (!!albumId) {
        album = albums[albumId];
    } else {
        album = null;
    }

    if (Array.isArray(album) && !!titleFilter) {
        album = album.filter(({ title }) => title.toLowerCase().includes(titleFilter.toLowerCase()))
    }

    return album;
};

export const getAlbumsForSelector = ({ albums }) => {
    return [
        'favorites',
        ...Object.keys(albums)
    ].map(albumId => ({ id: albumId, label: albumId }));
};
