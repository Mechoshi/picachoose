import {
    ALBUMS_GET,
    FAVORITES_ADD,
    FAVORITES_REMOVE,
    HISTORY_SET_ALBUM,
    HISTORY_SET_FILTER,
    HISTORY_SET_PHOTO
} from '../action-types';

export const getAlbums = () => async (dispatch, getState, ajax) => {
    const { data } = await ajax('photos');

    const albums = data.reduce((obj, photo) => {
        obj[photo.albumId] = obj[photo.albumId] || [];
        obj[photo.albumId].push(photo);
        return obj;
    }, {});

    dispatch({
        payload: albums,
        type: ALBUMS_GET
    });
};

export const addToFavorites = photo => dispatch => {
    dispatch({
        payload: photo,
        type: FAVORITES_ADD
    });
};

export const removeFromFavorites = photoId => dispatch => {
    dispatch({
        payload: photoId,
        type: FAVORITES_REMOVE
    });
};

export const selectAlbum = albumId => dispatch => {
    dispatch({
        payload: '',
        type: HISTORY_SET_FILTER
    });
    dispatch({
        payload: albumId,
        type: HISTORY_SET_ALBUM
    });
    window.scrollTo(0, 0);
};

export const setFilter = query => dispatch => {
    dispatch({
        payload: query,
        type: HISTORY_SET_FILTER
    })
};

export const expandPhoto = photo => dispatch => {
    dispatch({
        payload: photo,
        type: HISTORY_SET_PHOTO
    });
};
