import 'babel-polyfill';
import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';

import Layout from './containers/Layout/Layout';
import Loader from './components/Loader/Loader';
import { persistor, store } from './store/create-store';

render(
    (<Provider store={store}>
        <PersistGate loading={<Loader/>} persistor={persistor}>
            <Layout/>
        </PersistGate>
    </Provider>),
    document.getElementById('app')
);
