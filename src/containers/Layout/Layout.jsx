import React, { Component } from 'react';
import { connect } from 'react-redux';

import styles from './layout.scss';

import { getAlbums } from "../../store/actions/index";

import Header from '../Header/Header';
import PhotoGallery from '../PhotoGallery/PhotoGallery';


export class Layout extends Component {

    componentDidMount() {
        this.props.getAlbums();
    }

    render() {
        return (
            <main className={styles.App}>
                <Header/>
                <PhotoGallery/>
            </main>
        );
    }
}

export default connect(null, { getAlbums })(Layout);
