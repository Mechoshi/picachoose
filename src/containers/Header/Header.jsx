import React, { Component } from 'react';
import { connect } from 'react-redux';

import styles from './header.scss';
import { selectAlbum } from '../../store/actions';
import Button from '../../components/Button/Button';
import Logo from '../../components/Logo/Logo';

export class Header extends Component {

    showFavorites = () => this.props.selectAlbum('favorites');

    render() {
        const { albumId } = this.props;

        return (
            <header className={styles.HeaderContainer}>
                <Logo/>
                <Button className={`${styles.ShowFavorites} ${albumId === 'favorites' ? styles.Selected : ''}`}
                        onClick={this.showFavorites}
                        onKeyDown={this.showFavorites}
                        title={'Show Favorites'}/>
            </header>
        );
    }
}

const mapStateToProps = ({ albums, history: { albumId } }) => ({ albumId, albums: Object.keys(albums) });

export default connect(mapStateToProps, { selectAlbum })(Header);
