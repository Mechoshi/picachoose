import React, { Component } from 'react';
import { connect } from 'react-redux';

import styles from './photo-gallery.scss';

import {
    addToFavorites,
    expandPhoto,
    removeFromFavorites,
    selectAlbum,
    setFilter
} from '../../store/actions';

import { getAlbumsForSelector, selectPhotosToDisplay } from '../../store/selectors';

import Album from '../../components/Album/Album';
import Dropdown from '../../components/Dropdown';
import ExpandedPhoto from '../../components/ExpandedPhoto/ExpandedPhoto';
import Search from '../../components/Search/Search';
import ThumbnailList from '../../components/ThumbnailList/ThumbnailList';


export class PhotoGallery extends Component {

    selectAlbum = ({ id }) => this.props.selectAlbum(id);

    render() {
        const { album, albumsOptions, favorites, history: { albumId, photo, titleFilter } } = this.props;
        const { addToFavorites, expandPhoto, removeFromFavorites, selectAlbum, setFilter } = this.props;

        const scrollConfig = {
            adjustItems: true,
            scrollerOpacity: '0.2',
            scrollerWidth: '6px',
            scrollerColor: '#34abf3',
            scrollerBorderRadius: '10px',
            scrollerAlignment: 'right',
            scrollerOffset: '5px',
            scrollWindowHeight: '13rem'
        };

        return (
            <section className={styles.PhotoGalleryContainer}>
                <header className={styles.GalleryHeader}>
                    <div className={styles.AlbumSelectorContainer}>
                        <i className={styles.AlbumIcon}/>
                        <Dropdown options={albumsOptions}
                                  value={{ id: albumId, label: albumId }}
                                  placeholder='Select album'
                                  onChange={this.selectAlbum}
                                  className={styles.Dropdown}
                                  style={{ zIndex: '200' }}
                                  scrollerOptions={scrollConfig}
                                  selectedOverlayColor={'#34abf3'}
                                  component={Album}
                                  inputStyles={{
                                      height: '34px',
                                      padding: '1rem 0.5rem',
                                      color: '#21201f',
                                      opacity: '0.8',
                                      fontSize: '1rem',
                                      letterSpacing: '2px',
                                      backgroundColor: '#f0f8ff',
                                      borderRadius: '4px'
                                  }}
                        />
                    </div>
                    <Search id={'search-title'}
                            className={styles.Search}
                            label={'Search photos'}
                            onChange={({ target: { value } }) => setFilter(value)}
                            placeholder={'by title'}
                            value={titleFilter}/>
                </header>
                <ThumbnailList addToFavorites={addToFavorites}
                               album={album}
                               albumId={albumId}
                               elementDimensions={{
                                   width: 212,
                                   height: 332
                               }}
                               expandPhoto={expandPhoto}
                               favorites={favorites}
                               history={this.props.history}
                               removeFromFavorites={removeFromFavorites}
                               selectAlbum={selectAlbum}/>
                <ExpandedPhoto close={() => expandPhoto(null)}
                               goToAlbum={selectAlbum}
                               isFavorite={photo && !!favorites[photo.id]}
                               like={addToFavorites}
                               photo={photo}
                               unlike={removeFromFavorites}/>
            </section>
        );
    }
}

const mapStateToProps = state => {
    const { favorites, history } = state;
    return {
        album: selectPhotosToDisplay(state),
        albumsOptions: getAlbumsForSelector(state),
        favorites,
        history
    }
};

export default connect(
    mapStateToProps,
    {
        addToFavorites,
        expandPhoto,
        removeFromFavorites,
        selectAlbum,
        setFilter
    }
)(PhotoGallery);
