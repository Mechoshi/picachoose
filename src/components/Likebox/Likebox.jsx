import React, { Fragment } from 'react';

import styles from './likebox.scss';

export const Likebox = ({ className, isFavorite, item, like, unlike }) => {
    const id = `likebox-${item.id}`;
    let labelClasses = `${styles.Label}`;
    if (isFavorite) {
        labelClasses += ` ${styles.LikeIcon}`;
    } else {
        labelClasses += ` ${styles.UnlikeIcon}`;
    }
    if (className) {
        labelClasses += ` ${className}`
    }
    return (
        <Fragment>
            <label htmlFor={id}
                   tabIndex="0"
                   title={isFavorite ? 'Unlike' : 'Like'}
                   className={labelClasses}>
            </label>
            <input id={id}
                   className={styles.Input}
                   type="checkbox"
                   checked={isFavorite}
                   onChange={() => isFavorite ? unlike(item.id) : like(item)}>
            </input>
        </Fragment>
    );
};

export default Likebox;
