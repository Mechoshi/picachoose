import React from "react";

export const Loader = () => <h1>Loading...</h1>;

export default Loader;
