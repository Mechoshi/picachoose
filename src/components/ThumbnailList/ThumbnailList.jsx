import React from 'react';

import styles from "./thumbnail-list.scss";
import Thumbnail from '../Thumbnail/Thumbnail';

const positionsCalculator = (width, elWidth, elHeight, elCount) => {
    const positions = [];
    const numberOfElementsInRow = Math.floor(width / elWidth);
    const offsetLeft = (width - (numberOfElementsInRow * elWidth)) / 2;

    for (let i = 1; i <= elCount; i++) {
        let x = offsetLeft + ((i % numberOfElementsInRow || numberOfElementsInRow) - 1) * elWidth;
        let y = (Math.ceil(i / numberOfElementsInRow) - 1) * elHeight;
        positions.push({ x, y, transition: 500 + i * 30 });
    }

    return positions;
};


export class ThumbnailList extends React.Component {

    state = {
        positions: [],
        height: 0
    };

    setPositions = () => {
        const { album, elementDimensions } = this.props;
        const { offsetWidth } = this.el;
        if (album) {
            const { height, width } = elementDimensions;
            const numberOfElementsInRow = Math.floor(offsetWidth / width);
            const numberOfRows = Math.ceil(album.length / numberOfElementsInRow);
            this.setState({
                positions: positionsCalculator(offsetWidth, width, height, album.length),
                height: numberOfRows * height
            });
        }
    };

    render() {
        const { height, positions } = this.state;
        const { addToFavorites, expandPhoto, removeFromFavorites, selectAlbum } = this.props;
        const { album, favorites, history: { albumId, titleFilter } } = this.props;

        return (
            <section className={styles.ThumbnailListContainer}
                     ref={el => this.el = el}
                     style={{ height: `${height}px` }}>
                {
                    (Array.isArray(album) && album.length > 0)
                        ? album.map((photo, index) => <Thumbnail albumId={albumId}
                                                                 isFavorite={!!favorites[photo.id]}
                                                                 key={photo.id}
                                                                 like={addToFavorites}
                                                                 onSelect={expandPhoto}
                                                                 position={positions[index] || { x: 0, y: 0 }}
                                                                 photo={photo}
                                                                 selectAlbum={selectAlbum}
                                                                 titleFilter={titleFilter}
                                                                 unlike={removeFromFavorites}/>)
                        : <p className={styles.NoResults}>No photos to display</p>
                }
            </section>
        );
    }

    componentDidMount() {
        window.addEventListener('resize', this.setPositions);
        this.setPositions();
    }

    componentDidUpdate(prevProps) {
        if (prevProps.album !== this.props.album) {
            this.setPositions();
        }

        if (prevProps.albumId !== this.props.albumId) {
            this.setState(() => ({ positions: [] }), () => setTimeout(this.setPositions, 500));
        }
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.setPositions);
    }
}

export default ThumbnailList;
