import React, { Fragment } from 'react';

import styles from './filtered-title.scss';


export const FilteredTitle = ({ filter, title }) => {
    let filteredTitle = title;
    if (filter && title.indexOf(filter) >= 0) {
        const pre = title.substring(0, title.indexOf(filter));
        const post = title.substring(title.indexOf(filter) + filter.length);
        filteredTitle = (
            <Fragment>
                {pre}
                <span className={styles.FilteredPart}>{filter}</span>
                {post}
            </Fragment>
        );
    }

    return filteredTitle;
};

export default FilteredTitle;
