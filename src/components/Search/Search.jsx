import React from 'react';

import styles from './search.scss';

export const Search = ({ className, id, label, onChange, placeholder, value }) => {
    return (
        <div className={styles.SearchContainer + (` ${className}` || '')}>
          {/*  <label className={styles.Label} htmlFor={id} title={label}>{label}</label>*/}
            <label className={styles.LabelIcon} htmlFor={id} title={label}>
            </label>
            <input id={id}
                   className={styles.Input}
                   type="text"
                   placeholder={placeholder}
                   value={value}
                   onChange={onChange}/>
        </div>
    );
};

export default Search;
