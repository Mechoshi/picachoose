import React from 'react';

export const Button = ({ className, label, onClick, onKeyDown, title }) => {
    return (
        <button title={title} className={className} onClick={onClick} onKeyDown={onKeyDown}>
            {label}
        </button>
    );
};

export default Button;
