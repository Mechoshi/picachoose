import React from 'react';

import styles from './thumbnail.scss';

import FilteredTitle from '../FilteredTitle/FilteredTitle';
import Likebox from '../Likebox/Likebox';


export const Thumbnail = ({ albumId, isFavorite, like, onSelect, photo, position, selectAlbum, titleFilter, unlike }) => {
    const { id, thumbnailUrl, title } = photo;

    return (
        <article className={styles.Thumbnail} style={{
            transform: `translate3d(${position.x}px,${position.y}px,0)`,
            opacity: (position.x === 0 && position.y === 0) ? '0' : '1',
            transition: `all ${(position.x === 0 && position.y === 0) ? 0 : position.transition || 800}ms ease-in-out`
        }}>
            <figure className={styles.ImageContainer} role="button" tabIndex="0"
                    onClick={event => onSelect(photo)}
                    onKeyDown={event => {
                        if (event.key === 'Enter') {
                            onSelect(photo);
                        }
                    }}>
                <span className={styles.Expand}></span>
                <img className={styles.Image} src={thumbnailUrl} alt={`photo ${id}`}/>
            </figure>
            <figcaption className={styles.Title}><FilteredTitle filter={titleFilter} title={title}/>
            </figcaption>
            {
                albumId === 'favorites'
                    ? <button className={styles.GoToAlbum}
                              onClick={() => selectAlbum(`${photo.albumId}`)}
                              title={'Go to album'}>
                    </button>
                    : null
            }
            <Likebox className={styles.Likebox} isFavorite={isFavorite} item={photo} like={like} unlike={unlike}/>
        </article>
    );
};

export default Thumbnail;
