import React from 'react';

import styles from './overlay.scss';

export const Overlay = ({ onClick }) => {

    return (
        <div className={styles.OverlayContainer} onClick={onClick}>
        </div>
    )

};

export default Overlay;
