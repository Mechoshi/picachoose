import React from 'react';

import styles from './expanded-photo.scss';
import Likebox from '../Likebox/Likebox';

export const ExpandedPhoto = ({ close, goToAlbum, isFavorite, like, photo, unlike }) => {
    let template = null;
    if (photo) {
        template = (
            <div className={styles.ExpandedPhotoContainer} onClick={close}>
                <figure className={styles.Photo} onClick={(e) => e.stopPropagation()}>
                    <button className={styles.Close}
                            onClick={close}
                            title={'Close'}>
                    </button>
                    <button className={styles.GoToAlbum}
                            onClick={() => {
                                close();
                                goToAlbum(photo.albumId);
                            }}
                            title={'Go to album'}>
                    </button>
                    <Likebox className={styles.Likebox}
                             isFavorite={isFavorite}
                             item={photo}
                             like={like}
                             unlike={unlike}/>
                    <img src={photo.url} alt={`photo ${photo.id}`}/>
                    <figcaption className={styles.Title}>{photo.title}</figcaption>
                </figure>
            </div>
        );
    }

    return template;
};

export default ExpandedPhoto;
