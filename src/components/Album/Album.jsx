import React from 'react';

import styles from './album.scss';

export const Album = ({ id, label }) => {
    return (
        <div className={styles.AlbumContainer} id={id}>{label}</div>
    );
};

export default Album;
