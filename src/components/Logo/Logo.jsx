import React from 'react';

import styles from './logo.scss';

export const Logo = props => {
    return (
        <h1 className={styles.Logo}>picachoose</h1>
    );
};

export default Logo;
