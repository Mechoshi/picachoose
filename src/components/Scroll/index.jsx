import React, { Component } from 'react';
import { findDOMNode } from 'react-dom';

class Scroll extends Component {
    static OFFSET = 35;
    state = {
        scrollerClass: 'scroller',
        scrollerStyles: {
            position: 'absolute',
            [`${this.props.scrollerAlignment}`]: this.props.scrollerOffset,
            zIndex: 100,
            width: this.props.scrollerWidth,
            opacity: this.props.scrollerOpacity,
            backgroundColor: this.props.scrollerColor,
            borderRadius: this.props.scrollerBorderRadius,
            cursor: 'pointer',
            // See https://www.chromestatus.com/features/5093566007214080
            // and https://developers.google.com/web/updates/2017/01/scrolling-intervention
            touchAction: 'none'
        },
        style: {
            margin: '0px',
            padding: '0px',
            position: 'relative',
            overflow: 'hidden'
        },
        containerStyles: {
            width: `calc(100% + ${Scroll.OFFSET}px)`,
            height: this.props.scrollWindowHeight,
            overflowY: 'scroll',
            overflowX: 'hidden'
        }
    };

    children = null;
    config = {
        compensation: 0,
        moving: false
    };
    observer = null;
    shouldClone = true;

    startMovingScroller = event => {
        let pageY = 0;
        if (event.type === 'touchstart') {
            pageY = event.changedTouches[0].pageY;
        } else if (event.type === 'mousedown') {
            pageY = event.pageY;
        }
        event.preventDefault();
        event.stopPropagation();
        this.setState(state => {
            return {
                ...state,
                scrollerClass: 'scroller moving'
            }
        });
        const scrollerOffset = this.refs.scroller.style.transform ? Math.floor(parseFloat(this.refs.scroller.style.transform.split(',')[1])) : 0;
        this.config.compensation = pageY - scrollerOffset - this.refs.wrapper.offsetTop;
        this.config.moving = true;
    };

    setScrollerTranslate = event => {
        const st = this.container.scrollTop;
        const oh = this.container.offsetHeight;
        const sh = this.container.scrollHeight;
        const scrollerH = this.refs.scroller.offsetHeight;
        const top = ((oh - scrollerH) / (sh - oh)) * st;
        this.setState((state) => {
            return {
                ...state,
                scrollerStyles: {
                    ...state.scrollerStyles,
                    transform: `translate3d(0px,${parseInt(top)}px, 0px)`
                }
            }
        });
    };

    setContentScroll = y => {
        const oh = this.container.offsetHeight;
        const sh = this.container.scrollHeight;
        const scrollerH = this.refs.scroller.offsetHeight;
        this.container.scrollTop = ((sh - oh) / (oh - scrollerH)) * y;
    };

    adjustListItemsWidth(children) {
        Array.from(children).forEach(item => {
            if (!item.$$adjusted) {
                let { marginLeft, marginRight } = item.currentStyle || window.getComputedStyle(item);
                if (this.props.scrollerAlignment === 'right') {
                    marginRight = parseFloat(marginRight) + Scroll.OFFSET;
                    item.style.marginRight = `${marginRight}px`;
                } else if (this.props.scrollerAlignment === 'left') {
                    marginLeft = parseFloat(marginLeft) + Scroll.OFFSET;
                    item.style.marginLeft = `${marginLeft}px`;
                    // todo: compensate on the right
                }
                item.$$adjusted = true;
            }
        });
    }

    setScrollerHeight = () => {
        let { borderRight, margin, padding } = this.container.currentStyle || window.getComputedStyle(this.container);
        this.container.style.margin = '0px';
        this.container.style.padding = '0px';
        const oh = this.container.offsetHeight;
        const sh = this.container.scrollHeight;

        this.adjustListItemsWidth(this.container.children);

        const h = Math.floor(Math.pow(oh, 2) / sh);
        this.setState((state) => {
            return {
                ...state,
                scrollerStyles: {
                    ...state.scrollerStyles,
                    display: sh <= oh ? 'none' : 'block',
                    height: `${h}px`
                },
                style: {
                    ...state.style,
                    borderRight,
                    margin: margin === '0px' ? state.style.margin : margin,
                    padding: padding === '0px' ? state.style.padding : padding
                }
            }
        });
    };

    moveScroller = event => {
        if (this.config.moving) {
            let pageY = 0;
            if (event.type === 'touchmove') {
                pageY = event.changedTouches[0].pageY;
            } else if (event.type === 'mousemove') {
                pageY = event.pageY;
            }
            const max = this.refs.wrapper.clientHeight - this.refs.scroller.clientHeight;
            let y = pageY - this.refs.wrapper.offsetTop - this.config.compensation;
            y = y <= 0 ? 0 : y;
            y = y >= max ? max : y;
            this.setState((state) => {
                return {
                    ...state,
                    scrollerStyles: {
                        ...state.scrollerStyles,
                        transform: `translate3d(0px,${parseInt(y)}px, 0px)`
                    }
                }
            });
            this.setContentScroll(y);
        }
    };

    stopMovingScroller = event => {
        event.preventDefault();
        event.stopPropagation();
        this.setState(state => {
            return {
                ...state,
                scrollerClass: 'scroller'
            }
        });
        this.config.moving = false;
    };

    init = () => {
        document.addEventListener('mousemove', this.moveScroller);
        document.addEventListener('mouseup', this.stopMovingScroller);
        document.addEventListener('mouseleave', this.stopMovingScroller);
        window.addEventListener('resize', this.setScrollerHeight);

        const observerConfig = {
            attributes: true,
            characterData: true,
            childList: true,
            subtree: true
        };
        this.observer = new MutationObserver(this.setScrollerHeight);
        this.observer.observe(this.container, observerConfig);

        // Needed in the case of scrolling a React Component
        // After getting the DOM node the children array is
        // still empty without the timeout.
        setTimeout(() => {
            this.setScrollerHeight();
            this.setScrollerTranslate();
        }, 0);

        this.setState((state) => {
            return {
                ...state,
                scrollerStyles: {
                    ...state.scrollerStyles,
                    transform: `translate3d(0px, 0px, 0px)`
                }
            }
        });
    };

    clean = () => {
        document.removeEventListener('mousemove', this.moveScroller);
        document.removeEventListener('mouseup', this.stopMovingScroller);
        document.removeEventListener('mouseleave', this.stopMovingScroller);
        window.removeEventListener('resize', this.setScrollerHeight);
        this.observer && this.observer.disconnect();
        this.observer = null;
    };

    render() {
        if (this.props.children && this.shouldClone) {
            this.children = React.cloneElement(React.Children.only(this.props.children), {
                style: this.state.containerStyles,
                onScroll: this.setScrollerTranslate,
                ref: child => {
                    this.container = (child instanceof Node) ? child : findDOMNode(child);
                    if (!(child instanceof Node) && this.container !== null) {
                        this.container.style.width = this.state.containerStyles.width;
                        this.container.style.overflowY = this.state.containerStyles.overflowY;
                        this.container.onscroll = this.setScrollerTranslate
                    }
                }
            });
            this.shouldClone = false;
        } else if (!this.props.children) {
            this.children = null;
            this.clean();
            this.shouldClone = true;
        }
        return (
            this.children
                ?
                <div ref="wrapper" className="scroller-wrapper" style={this.state.style}>
                    <div className={this.state.scrollerClass}
                         ref="scroller"
                         style={this.state.scrollerStyles}
                         onMouseDown={this.startMovingScroller}
                         onTouchStart={this.startMovingScroller}
                         onTouchMove={this.moveScroller}
                         onTouchEnd={this.stopMovingScroller}
                    >
                    </div>
                    {this.children}
                </div>
                :
                null
        );
    }

    componentDidMount() {
        if (this.props.children) {
            this.init();
        }
    }

    shouldComponentUpdate(nextProps, nextState) {
        if (nextProps.children && this.props.children !== nextProps.children) {
            this.shouldClone = true;
        }
        return true;
    }

    componentDidUpdate(prevProps, prevState) {
        if (this.props.children && this.props.children !== prevProps.children) {
            this.clean();
            this.init();
        }
    }

    componentWillUnmount() {
        this.clean();
        this.children = null;
    }

}

export default Scroll;
