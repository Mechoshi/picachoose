import React, { Component } from 'react';
import { CSSTransition } from 'react-transition-group';

import styles from './Dropdown.scss';
import Scroll from '../Scroll';

class Dropdown extends Component {
    state = {
        filter: '',
        id: this.props.id || 'id',
        inputValue: '',
        label: this.props.label || 'label',
        option: -1,
        showOptions: false
    };

    showOptions() {
        this.setState({ option: -1, showOptions: true });
    };

    onInputChange({ target }) {
        const { id, label } = this.state;
        const { autocomplete, onChange, onFilter } = this.props;
        const { value } = target;

        this.setState({ filter: value, inputValue: value, option: -1 });
        autocomplete && onFilter(value);
        onChange({ [id]: null, [label]: '' });
    }

    onClickSelection(item) {
        this.setState({ filter: '', showOptions: false });
        this.props.onChange(item);
    };

    getFilteredOptions() {
        const { filter, label } = this.state;

        return this.props.options.filter(opt => (opt[label].toLowerCase()).indexOf(filter.toLowerCase()) > -1);
    };

    hideOptions = ({ target }) => {
        const { label } = this.state;
        const { value } = this.props;
        const inputValue = (value && value[label]) ? value[label] : '';

        if (this.refs.el && !this.refs.el.contains(target)) {
            this.setState({ filter: '', inputValue: inputValue, option: -1, showOptions: false });
        }
    };

    onInputKeyDown({ keyCode, target: { value } }) {
        const { label, showOptions } = this.state;
        const { autocomplete, onChange, options } = this.props;

        if (!showOptions) {
            // If not opened input.nextElementSibling will not be present in the DOM
            return this.showOptions();
        }
        const input = this.refs.inputEl;

        if (input.nextElementSibling === null) {
            return;
        }

        const sel = input.nextElementSibling.children[1];
        const filteredOptions = autocomplete ? options : this.getFilteredOptions();

        switch (keyCode) {
            case 13:
                for (let i = 0; i < filteredOptions.length; i++) {
                    if (value.toLowerCase() === filteredOptions[i][label].toLowerCase() && this.state.option === -1) {
                        onChange(filteredOptions[i]);
                        this.setState({ option: -1 });
                        return;
                    }
                }
                if (this.state.option > -1) {
                    onChange(filteredOptions[this.state.option]);
                    this.setState({ option: -1 });
                    return;
                }
                this.setState({ filter: '', inputValue: '', option: -1 });
                break;

            case 40:
                if (this.state.option === -1) {
                    sel.scrollTop = 0;
                }

                if (filteredOptions.length > 0) {
                    const cb = () => {
                        if (this.state.option >= this.getNumberOfOptionsToFit(input) && this.state.option < filteredOptions.length) {
                            sel.scrollTop += this.getChildHeight(sel.children[this.state.option].children[1]);
                        }
                    };

                    if (this.state.option === filteredOptions.length - 1) {
                        this.setState(prevState => ({ option: 0 }), cb);
                        sel.scrollTop = 0;
                    } else {
                        this.setState(prevState => ({ option: prevState.option + 1 }), cb);
                    }
                }
                break;

            case 38:
                if (filteredOptions.length > 0) {
                    const cb = () => {
                        if (this.state.option >= this.getNumberOfOptionsToFit(input)) {
                            sel.scrollTop -= this.getChildHeight(sel.children[this.state.option].children[1]);
                        } else {
                            sel.scrollTop = 0;
                        }
                    };

                    if (this.state.option === 0) {
                        this.setState(prevState => ({ option: -1 }), cb);
                    } else if (this.state.option > 0) {
                        this.setState(prevState => ({ option: prevState.option - 1 }), cb);
                    }
                }
                break;

            default:
                break;
        }
    }

    getNumberOfOptionsToFit(input) {
        const sel = input.nextElementSibling.children[1];
        let optionsToFit = 0;
        let sumOfChildrenHeights = 0;

        for (let i = 0; i < sel.children.length; i++) {
            // The actual element which height is needed
            // is nested in a wrapper element and is the
            // second child. The first child is the
            // overlay indicating the selected item.
            if (sumOfChildrenHeights + this.getChildHeight(sel.children[i].children[1]) < sel.clientHeight) {
                sumOfChildrenHeights += this.getChildHeight(sel.children[i].children[1]);
                optionsToFit++;
            } else {
                break;
            }
        }

        return optionsToFit;
    };

    getChildHeight(child) {
        let { marginBottom, marginTop } = child.currentStyle || window.getComputedStyle(child);
        return child.clientHeight + parseFloat(marginBottom) + parseFloat(marginTop);
    }

    listItemOnKeyDown(event, option) {
        const filteredOptions = this.props.autocomplete ? this.props.options : this.getFilteredOptions();
        const input = this.refs.inputEl;
        const sel = input.nextElementSibling.children[1];

        if (!(event.keyCode === 9)) {

            switch (event.keyCode) {
                case 13:
                    this.onClickSelection(option);
                    break;

                case 38:
                    if (filteredOptions.length > 0) {
                        const cb = () => {
                            if (this.state.option >= this.getNumberOfOptionsToFit(input)) {
                                sel.scrollTop -= this.getChildHeight(sel.children[this.state.option].children[1]);
                            } else {
                                sel.scrollTop = 0;
                            }
                            if (this.state.option > -1) {
                                sel.children[this.state.option].focus();
                            } else {
                                input.focus();
                            }
                        };

                        if (this.state.option === 0) {
                            this.setState(prevState => ({ option: -1 }), cb);
                        } else if (this.state.option > 0) {
                            this.setState(prevState => ({ option: prevState.option - 1 }), cb);
                        }
                    }
                    break;

                case 40:
                    if (this.state.option === -1) {
                        sel.scrollTop = 0;
                    }

                    if (filteredOptions.length > 0) {
                        const cb = () => {
                            if (this.state.option >= this.getNumberOfOptionsToFit(input) && this.state.option < filteredOptions.length) {
                                sel.scrollTop += this.getChildHeight(sel.children[this.state.option].children[1]);
                            }
                            sel.children[this.state.option].focus();
                        };

                        if (this.state.option === filteredOptions.length - 1) {
                            this.setState(prevState => ({ option: 0 }), cb);
                            sel.scrollTop = 0;
                        } else {
                            this.setState(prevState => ({ option: prevState.option + 1 }), cb);
                        }
                    }
                    break;
            }

            event.preventDefault();
        }
    };

    render() {
        const { id, label, showOptions } = this.state;
        const {
            autocomplete, className, component, componentProps,
            inputStyles, options, placeholder, scrollerOptions,
            selectedOverlayColor, value
        } = this.props;
        const filteredOptions = autocomplete ? options : this.getFilteredOptions();
        const Cmp = component || null;
        const cmpProps = componentProps || {};

        const listItems = filteredOptions.map((opt, i) => {
            const isSelected = value && opt[id] === value[id];
            return (
                <li onFocus={(e) => {
                    e.stopPropagation();
                    this.setState({ option: i });
                }}
                    id={`${opt[label]}_${opt[id]}`}
                    aria-labelledby={`${opt[label]}_${opt[id]}_aria`}
                    data-value={opt[label]}
                    tabIndex="0"
                    role="option"
                    aria-selected={isSelected}
                    key={opt[id]}
                    onClick={() => this.onClickSelection(opt)}
                    onKeyDown={event => this.listItemOnKeyDown(event, opt)}
                    className={(isSelected ? styles.Selected : '') + (this.state.option === i ? ` ${styles.KeySelected}` : '')}
                >
                    <span className={styles.SelectedOverlay}
                          style={{ backgroundColor: selectedOverlayColor }}>
                    </span>
                    {Cmp ? <Cmp {...cmpProps} ariaId={`${opt[label]}_${opt[id]}_aria`} {...opt}/> : null}
                </li>
            )
        });

        const selectedItem = value && options.find(opt => value[id] === opt[id]);
        let activeDescendantId = '';
        if (selectedItem) {
            activeDescendantId = `${selectedItem[label]}_${selectedItem[id]}`;
        }

        const list = (
            <ul role="listbox"
                aria-expanded={showOptions}
                aria-live='polite'
                aria-activedescendant={activeDescendantId}
                tabIndex="0"
                onFocus={({ target }) => {
                    const option = target.querySelector('[aria-selected="true"]');
                    if (option) {
                        option.focus();
                        this.setState({ option: options.findIndex(opt => opt[id] === value[id]) });
                    } else {
                        if (target.children.length > 0) {
                            this.setState({ option: 0 });
                            target.children[0].focus();
                        }
                    }
                }}>
                {listItems}
            </ul>
        );

        let inputValue = '';
        for (let i = 0; i < options.length; i++) {
            if (value && options[i][id] === value[id]) {
                inputValue = options[i][label];
                break;
            }
        }

        return (
            <div ref='el' style={this.props.style}
                 className={`${className} ${styles.Dropdown}`}
                 onBlur={({ relatedTarget }) => {
                     if (!this.refs.el.contains(relatedTarget)) {
                         this.hideOptions({ target: relatedTarget });
                     }
                 }}>
                <div className={styles.DropdownInputsWrapper}>
                    <input
                        aria-haspopup="listbox"
                        ref="inputEl"
                        type="text"
                        style={inputStyles}
                        placeholder={placeholder}
                        value={inputValue || this.state.inputValue}
                        onChange={e => this.onInputChange(e)}
                        onClick={e => this.showOptions(e)}
                        onKeyDown={e => this.onInputKeyDown(e)}
                    />
                    <CSSTransition
                        in={showOptions}
                        unmountOnExit={true}
                        timeout={200}
                        classNames={{
                            enter: styles.DropDownEnter,
                            enterActive: styles.DropDownEnterActive,
                            exit: styles.DropDownExit,
                            exitActive: styles.DropDownExitActive
                        }}>
                        <Scroll {...scrollerOptions} adjustItems={true}>
                            {filteredOptions.length > 0 ? list : null}
                        </Scroll>
                    </CSSTransition>
                </div>
            </div>
        );
    }

    componentDidMount() {
        document.addEventListener('click', this.hideOptions);
    }

    shouldComponentUpdate({ value: nextValue }, nextState, nextContext) {
        const { id, label } = this.state;
        const { value: currentValue } = this.props;

        if (nextValue && nextValue[id] !== null && (!currentValue || nextValue[id] !== currentValue[id])) {
            this.setState({ filter: '', inputValue: nextValue[label], showOptions: false });
        }

        return true;
    }

    componentWillUnmount() {
        document.removeEventListener('click', this.hideOptions);
    }

}

export default Dropdown;
