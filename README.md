# Picachoose

Choose your favourite pictures

## Technologies

* React 16
* Redux 4
* Redux-persist
* Redux-thunk
* Axios
* Scss
* Webpack 4
* Webpack-dev-server 3
* Babel
* ES6/ES7 features
* Node 9.11
* Yarn
* Git Flow

## Start developing
* Clone the repo
* Install dependencies
```
yarn install
```
* Start the `development` mode
```
yarn dev
```
* The app will automatically open in the default browser on `http://localhost:8080`
* The app will be rebuilt and the browser will reload when changes are saved

## Build for production
* Run
```
yarn build
```
* All resources will be saved in the `dist` folder

## Check the production build
* To start a simple `express` server run
```
yarn check:build
```
* Open the app on `http://localhost:3000`